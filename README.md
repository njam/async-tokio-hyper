async-tokio-hyper
=================

Rust example showing how to send an HTTP request with tokio and hyper using the new _async/await_ syntax.
