use std::error::Error;
use std::str::FromStr;

use futures::TryStreamExt;
use http::{Response, Uri};
use hyper::{Body, Chunk, Client};

#[tokio::main]
async fn main() {
    let url = Uri::from_str("http://example.com").unwrap();
    match fetch_url(url).await {
        Ok(body) => println!("{}", body),
        Err(error) => eprintln!("ERROR: {}", error)
    }
}

async fn fetch_url(url: Uri) -> Result<String, Box<dyn Error>> {
    let client = Client::new();
    let response: Response<Body> = client.get(url).await?;
    let body_chunk: Chunk = response.into_body().try_concat().await?;
    let body_string: String = String::from_utf8(body_chunk.to_vec())?;
    Ok(body_string)
}
